angular.module('horarios.filtros', [])

    .controller('FiltrosCtrl', ['$scope', 'object', function($scope, object) {
        $scope.filtros = filtros;
        $scope.dias = dias;
        $scope.diasPorHoras = [];
        $scope.filterData = object.get();
        var avoidDayTemp = [];
        var avoidHoursTemp = [];
        var avgProfCalif = undefined;

        $scope.$watch('filterData', function () {
            object.set($scope.filterData);
        },true);

        $scope.$watch('filtros[0].enabled', function () {
            if(!filtros[0].enabled){
                avoidDayTemp = $scope.filterData.filters.avoidDay;
                $scope.filterData.filters.avoidDay = [];
            }
            else {
                $scope.filterData.filters.avoidDay = avoidDayTemp;
            }
        });

        $scope.$watch('filtros[1].enabled', function() {
            if(!filtros[1].enabled){
                avoidHoursTemp = $scope.filterData.filters.avoidHours;
                $scope.filterData.filters.avoidHours = [];
            }
            else{
                $scope.filterData.filters.avoidHours = avoidHoursTemp;
            }
        });

        $scope.$watch('filtros[2].enabled', function() {
            if(!filtros[2].enabled){
                avgProfCalif = $scope.filterData.filters.misProfMinAvgScore;
                $scope.filterData.filters.misProfMinAvgScore = undefined;
            }
            else{
                $scope.filterData.filters.misProfMinAvgScore = avgProfCalif;
            }
        });

        $scope.addInterval = function(){
            $scope.filterData.filters.avoidHours.push({days: [], startTime: defaultIni, endTime: defaultFin});
            var arre = [];
            for(var i=1; i<=5; i++)
                arre.push({key: i, enabled: false});
            $scope.diasPorHoras.push(arre);
        };

        $scope.removeInterval = function(index){
            $scope.filterData.filters.avoidHours.splice(index, 1);
        };

        $scope.changeEndTime = function (interval) {
            if(interval.endTime<interval.startTime)
                interval.endTime = interval.startTime;
        };

        $scope.changeStartTime = function(interval){
            if(interval.endTime<interval.startTime)
                interval.startTime = interval.endTime;
        };

        $scope.addRemoveDay = function(dayKey, interval){
            if(interval.days===undefined || interval.days.indexOf(dayKey)<0) {
                interval.days.push(dayKey);
                $scope.diasPorHoras[interval.$index][dayKey-1].enabled=true;
            }
            else {
                interval.days.splice(interval.days.indexOf(dayKey), 1);
                $scope.diasPorHoras[interval.$index][dayKey-1].enabled=false;
            }
        };

        $scope.addRemoveWholeDay = function(dayKey){
            var index = $scope.filterData.filters.avoidDay.indexOf(dayKey);
            if(index<0)
                $scope.filterData.filters.avoidDay.push(dayKey);
            else
                $scope.filterData.filters.avoidDay.splice(index,1);
        };


    }]);