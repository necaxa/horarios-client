'use strict';

angular.module('horarios', [
    'ui.calendar',
    'ui.bootstrap',
    'horarios.controller',
    'horarios.filtros',
    'horarios.carreras',
    'horarios.materias',
    'ngRoute'
    //'ngAnimate'
])

    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/carreras', {
                    templateUrl: 'carreras/carreras.html'
                })
                .when('/filtros', {
                    templateUrl: 'filtros/filtros.html'
                })
                .when('/materias', {
                    templateUrl: 'materias/materias.html'
                })
                .when('/clases', {})
                .otherwise({redirectTo: '/carreras'});
        }])