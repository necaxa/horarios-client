angular.module('horarios.carreras', [])
    .controller('CarrerasCtrl', ['$scope', 'object',function($scope, object){
        $scope.filterData = object.get();
        $scope.carreras = carreras;

        $scope.$watch('filterData', function () {
            object.set($scope.filterData);
        },true);

        $scope.addRemoveCarrera = function(carrera){
            if(carrera.enabled)
                $scope.filterData.carreras.push(carrera);
            else
                $scope.filterData.carreras.splice($scope.filterData.carreras.indexOf(carrera.name),1);
        }

    }]);