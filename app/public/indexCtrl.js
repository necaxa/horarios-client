'use strict';

angular.module('horarios.controller', [])
    .controller('IndexCtrl', ['$scope', '$compile', '$timeout', '$route', '$location', 'uiCalendarConfig', '$http', 'object', function($scope, $compile, $timeout, $route, $location, uiCalendarConfig, $http, object) {

        $scope.filterData = object.get();
        $scope.selectedTab = 1;
        $scope.sourceCal1 = [];
        $scope.sourceCal2 = [];
        $scope.sourceCal3 = [];
        $scope.combFound1 = undefined;
        $scope.combFound2 = undefined;
        $scope.combFound3 = undefined;
        $scope.loading1 = false;
        $scope.loading2 = false;
        $scope.loading3 = false;
        $scope.error1 = false;
        $scope.error2 = false;
        $scope.error3 = false;
        $scope.pristine1 = true;
        $scope.pristine2 = true;
        $scope.pristine3 = true;
        $scope.data1 = [];
        $scope.data2 = [];
        $scope.data3 = [];
        $scope.indexCal1 = 0;
        $scope.indexCal2 = 0;
        $scope.indexCal3 = 0;
        var combinations1 = [];
        var combinations2 = [];
        var combinations3 = [];
        var mustHaveGroups1 = [];
        var mustHaveGroups2 = [];
        var mustHaveGroups3 = [];

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        /* config object */
        $scope.uiConfig = {
            calendar:{
                height: "auto",
                defaultView: 'agendaWeek',
                header: false,
                allDaySlot: false,
                slotEventOverlap: false,
                editable: false,
                /*eventClick: $scope.alertOnEventClick,
                eventRender: $scope.eventRender,*/
                hiddenDays: [0],
                minTime: '07:00:00',
                maxTime: '22:00:00',
                dayNamesShort : ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"]
            }
        };

        $scope.$on('$routeChangeSuccess', function(){
            $scope.selectedTool = $location.$$path.replace('/', '');
        });

        $scope.generarHorario = function(numcal){

            switch (numcal) {
                case 1:
                    $scope.loading1 = true;
                    $scope.error1 = false;
                    $scope.filterData.filters.mustHaveGroups = mustHaveGroups1;
                    break;
                case 2:
                    $scope.loading2 = true;
                    $scope.error2 = false;
                    $scope.filterData.filters.mustHaveGroups = mustHaveGroups2;
                    break;
                case 3:
                    $scope.loading3 = true;
                    $scope.error3 = false;
                    $scope.filterData.filters.mustHaveGroups = mustHaveGroups3;
                    break;
            }

            $scope.filterData = object.get();

            console.log(JSON.stringify($scope.filterData));
            $http.post('https://horarios-itam-server.herokuapp.com/generateSchedule', $scope.filterData).success(function(data){
                console.log(data);
                crearEventos(data, numcal);
                addPushPins(data, numcal);
                switch (numcal){
                    case 1:
                        $scope.loading1 = false;
                        $scope.error1 = false;
                        $scope.pristine1 = false;
                        $scope.combFound1 = data.length;
                        while($scope.sourceCal1.length>0)
                            $scope.sourceCal1.pop();
                        $scope.sourceCal1.push(combinations1[0]);
                        $scope.data1 = data;
                        break;
                    case 2:
                        $scope.loading2 = false;
                        $scope.error2 = false;
                        $scope.pristine2 = false;
                        $scope.combFound2 = data.length;
                        while($scope.sourceCal2.length>0)
                            $scope.sourceCal2.pop();
                        $scope.sourceCal2.push(combinations2[0]);
                        $scope.data2 = data;
                        break;
                    case 3:
                        $scope.loading3 = false;
                        $scope.error3 = false;
                        $scope.pristine3 = false;
                        $scope.combFound3 = data.length;
                        while($scope.sourceCal3.length>0)
                            $scope.sourceCal3.pop();
                        $scope.sourceCal3.push(combinations3[0]);
                        $scope.data3 = data;
                        break;
                }

            }).catch(function(e){
                switch(numcal) {
                    case 1:
                        $scope.loading1 = false;
                        $scope.error1 = true;
                        break;
                    case 2:
                        $scope.loading2 = false;
                        $scope.error2 = true;
                        break;
                    case 3:
                        $scope.loading3 = false;
                        $scope.error3 = true;
                        break;
                }
                console.log(JSON.stringify(e));
            });

        };

        var crearEventos = function(data, numcal){
            var color, textColor;
            switch (numcal) {
                case 1:
                    combinations1 = [];
                    color = '#337AB7';
                    textColor = 'white';
                    break;
                case 2:
                    combinations2 = [];
                    color = '#F0AD4E';
                    textColor = 'black';
                    break;
                case 3:
                combinations3 = [];
                color = '#D9534F';
                textColor = 'yellow';
                break;
            }
            data.forEach(function(comb){
                var source = {
                    color: color,
                    textColor: textColor,
                    events: []
                };
                comb.forEach(function(clase){
                    createObject(source,clase);
                    if(clase.laboratory!==undefined)
                        createObject(source, clase.laboratory);
                    if(clase.alternative!==undefined)
                        createObject(source, clase.alternative);
                });
                switch (numcal) {
                    case 1:
                        combinations1.push(source);
                        break;
                    case 2:
                        combinations2.push(source);
                        break;
                    case 3:
                        combinations3.push(source);
                        break;
                }
            });
        };

        var createObject = function(source, clase){
            var days = clase.days.split(",");
            days.forEach(function(day) {
                for (var repeat = -1; repeat < 1; repeat++) {
                    var obj = {};
                    obj.type = 'party';
                    obj.title = clase.name.substring(0,10) + " - " + clase.teacher.name.substring(0,10);
                    var times = clase.schedule.split("-");
                    var start = times[0].split(":");
                    var end = times[1].split(":");
                    switch (day) {
                        case "LU":
                            setStartEnd(obj, start, end, 1, repeat);
                            break;
                        case "MA":
                            setStartEnd(obj, start, end, 2, repeat);
                            break;
                        case "MI":
                            setStartEnd(obj, start, end, 3, repeat);
                            break;
                        case "JU":
                            setStartEnd(obj, start, end, 4, repeat);
                            break;
                        case "VI":
                            setStartEnd(obj, start, end, 5, repeat);
                            break;
                    }
                    source.events.push(obj);
                }
            });
        };

        var setStartEnd = function(obj, start, end, numday, repeat){
            var tempStart = new Date();
            var tempEnd = new Date();
            tempStart.setDate(tempStart.getDate() + ((numday + 7 - tempStart.getDay()) % 7) + (7 * repeat));
            tempStart.setHours(start[0]);
            tempStart.setMinutes(start[1]);
            tempStart.setSeconds(0);
            obj.start = tempStart;
            tempEnd.setDate(tempEnd.getDate() + ((numday + 7 - tempEnd.getDay()) % 7) + (7 * repeat));
            tempEnd.setHours(end[0]);
            tempEnd.setMinutes(end[1]);
            tempEnd.setSeconds(0);
            obj.end = tempEnd;
        };

        /* alert on eventClick */
        $scope.alertOnEventClick = function( date, jsEvent, view){
            console.log(date.title + ' was clicked ');
        };

        /* Change View */
        $scope.renderCalendar = function(calendar) {
            switch(calendar){
                case 'myCalendar1':
                    $scope.selectedTab = 1;
                    break;
                case 'myCalendar2':
                    $scope.selectedTab = 2;
                    break;
                case 'myCalendar3':
                    $scope.selectedTab = 3;
                    break;
            }
            $timeout(function() {
                if(uiCalendarConfig.calendars[calendar]){
                    uiCalendarConfig.calendars[calendar].fullCalendar('render');
                }
            });
        };
        /* Render Tooltip */
        $scope.eventRender = function( event, element, view ) {
            element.attr({'tooltip': event.title,
                'tooltip-append-to-body': true});
            $compile(element)($scope);
        };

        $scope.prevComb = function(numcal){
            switch(numcal){
                case 1:
                    if(combinations1.length>0) {
                        $scope.indexCal1--;
                        if (combinations1[$scope.indexCal1] === undefined)
                            $scope.indexCal1 = combinations1.length - 1;
                        $scope.sourceCal1.splice(0, 1);
                        $scope.sourceCal1.push(combinations1[$scope.indexCal1]);
                    }
                    break;
                case 2:
                    if(combinations2.length>0) {
                        $scope.indexCal2--;
                        if (combinations2[$scope.indexCal2] === undefined)
                            $scope.indexCal2 = combinations2.length - 1;
                        $scope.sourceCal2.splice(0, 1);
                        $scope.sourceCal2.push(combinations2[$scope.indexCal2]);
                    }
                    break;
                case 3:
                    if(combinations3.length>0) {
                        $scope.indexCal3--;
                        if (combinations3[$scope.indexCal3] === undefined)
                            $scope.indexCal3 = combinations3.length - 1;
                        $scope.sourceCal3.splice(0, 1);
                        $scope.sourceCal3.push(combinations3[$scope.indexCal3]);
                    }
                    break;
            }
        };

        $scope.nextComb = function(numcal){
            switch(numcal){
                case 1:
                    if(combinations1.length>0) {
                        $scope.indexCal1++;
                        if (combinations1[$scope.indexCal1] === undefined)
                            $scope.indexCal1 = 0;
                        $scope.sourceCal1.splice(0, 1);
                        $scope.sourceCal1.push(combinations1[$scope.indexCal1]);
                    }
                    break;
                case 2:
                    if(combinations2.length>0) {
                        $scope.indexCal2++;
                        if (combinations2[$scope.indexCal2] === undefined)
                            $scope.indexCal2 = 0;
                        $scope.sourceCal2.splice(0, 1);
                        $scope.sourceCal2.push(combinations2[$scope.indexCal2]);
                    }
                    break;
                case 3:
                    if(combinations3.length>0) {
                        $scope.indexCal3++;
                        if (combinations3[$scope.indexCal3] === undefined)
                            $scope.indexCal3 = 0;
                        $scope.sourceCal3.splice(0, 1);
                        $scope.sourceCal3.push(combinations3[$scope.indexCal3]);
                    }
                    break;
            }
        };

        $scope.addRemoveMustHaveGroups = function (event, numcal) {
            if (event.selected === undefined || !event.selected)
                event.selected = true;
            else
                event.selected = false;
            switch (numcal) {
                case 1:
                    var i = 0;
                    while (i < mustHaveGroups1.length && mustHaveGroups1[i].indexOf(event.key) === -1)
                        i++;
                    //encontro uno con la misma materia
                    if (i !== mustHaveGroups1.length) {
                        //si es diferente el grupo, agrega el nuevo grupo
                        if (mustHaveGroups1[i].indexOf(event.groupNum) === -1)
                            mustHaveGroups1.push(event.department + "-" + event.key + "-" + event.groupNum);
                        //si es igual, quita el grupo
                        var prevPin = mustHaveGroups1.splice(i, 1)[0];
                        removePushPin(prevPin, 1);
                    }
                    else
                        mustHaveGroups1.push(event.department + "-" + event.key + "-" + event.groupNum);
                    break;
                case 2:
                    var i = 0;
                    while (i < mustHaveGroups2.length && mustHaveGroups2[i].indexOf(event.key) === -1)
                        i++;
                    //encontro uno con la misma materia
                    if (i !== mustHaveGroups2.length) {
                        //si es diferente el grupo, agrega el nuevo grupo
                        if (mustHaveGroups2[i].indexOf(event.groupNum) === -1)
                            mustHaveGroups2.push(event.department + "-" + event.key + "-" + event.groupNum);
                        //si es igual, quita el grupo
                        var prevPin = mustHaveGroups1.splice(i, 1)[0];
                        removePushPin(prevPin, 2);
                    }
                    else
                        mustHaveGroups2.push(event.department + "-" + event.key + "-" + event.groupNum);
                    break;
                case 3:
                    var i = 0;
                    while (i < mustHaveGroups3.length && mustHaveGroups3[i].indexOf(event.key) === -1)
                        i++;
                    //encontro uno con la misma materia
                    if (i !== mustHaveGroups3.length) {
                        //si es diferente el grupo, agrega el nuevo grupo
                        if (mustHaveGroups3[i].indexOf(event.groupNum) === -1)
                            mustHaveGroups3.push(event.department + "-" + event.key + "-" + event.groupNum);
                        //si es igual, quita el grupo
                        var prevPin = mustHaveGroups1.splice(i, 1)[0];
                        removePushPin(prevPin, 3);
                    }
                    else
                        mustHaveGroups3.push(event.department + "-" + event.key + "-" + event.groupNum);
                    break;
            }
        }

        var addPushPins = function (data, numcal) {
            data.forEach(function (comb) {
                comb.forEach(function (event) {
                    var key = event.department + "-" + event.key + "-" + event.groupNum;
                    switch (numcal) {
                        case 1:
                            if (mustHaveGroups1.indexOf(key) !==-1)
                                event.selected = true;
                            break;
                        case 2:
                            if (mustHaveGroups2.indexOf(key) !== -1)
                                event.selected = true;
                            break;
                        case 3:
                            if (mustHaveGroups3.indexOf(key) !== -1)
                                event.selected = true;
                            break;
                    };
                });
            });
        };

        var removePushPin = function (pin, numcal) {
            switch (numcal) {
                case 1:
                    $scope.data1.forEach(function (comb) {
                        comb.forEach(function (event) {
                            var key = event.department + "-" + event.key + "-" + event.groupNum;
                            if (key === pin)
                                event.selected = false;
                        })
                    });
                    break;
                case 2:
                    $scope.data2.forEach(function (comb) {
                        comb.forEach(function (event) {
                            var key = event.department + "-" + event.key + "-" + event.groupNum;
                            if (key === pin)
                                event.selected = false;
                        })
                    });
                    break;
                case 3:
                    $scope.data3.forEach(function (comb) {
                        comb.forEach(function (event) {
                            var key = event.department + "-" + event.key + "-" + event.groupNum;
                            if (key === pin)
                                event.selected = false;
                        })
                    });
                    break;
            }
        }

    }])

    .factory('object', function(){
        var filterData = {
            carreras: [],
            courses:[],
            filters:{
                avoidDay: [],
                avoidHours: [],
                mustHaveGroups: []
            }
        };
        var get = function () {
            return filterData;
        };

        var set = function (updated) {
            filterData = updated;
        };

        var reset = function () {
            filterData = {
                carreras: [],
                courses:[],
                filters:{
                    avoidDay: [],
                    avoidHours: [],
                    mustHaveGroups: []
                }
            };
        };

        return {
            get: get,
            set: set,
            reset: reset
        };
    });


///CRAP may be needed later


/*$scope.changeTo = 'Hungarian';*/
/*
 $scope.changeLang = function() {
 if($scope.changeTo === 'Hungarian'){
 $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
 $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
 $scope.changeTo= 'English';
 } else {
 $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
 $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
 $scope.changeTo = 'Hungarian';
 }
 };*/

/* Change View
 $scope.changeView = function(view,calendar) {
 uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
 };
 */

/* alert on Drop
 $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
 $scope.alertMessage = ('Event Dropped to make dayDelta ' + delta);
 };
 */
/* alert on Resize
 $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
 $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
 };
 */

/* event source that pulls from google.com
 $scope.eventSource = {
 url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
 className: 'gcal-event',           // an option!
 currentTimezone: 'America/Chicago' // an option!
 };
 */

/* add and removes an event source of choice
 $scope.addRemoveEventSource = function(sources,source) {
 console.log('clicked');
 var canAdd = 0;
 angular.forEach(sources,function(value, key){
 if(sources[key] === source){
 sources.splice(key,1);
 canAdd = 1;
 }
 });
 if(canAdd === 0){
 sources.push(source);
 }
 };
 */

/*
 /* event source that contains custom events on the scope */
/*
$scope.events = [
    {title: 'All Day Event',start: new Date(y, m, 1)},
    {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
    {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
    {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
    {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
    {title: 'Click for Google',start: new Date(y, m, d),end: new Date(y, m, 29),url: 'http://google.com/'}
];
 */
/*$scope.calEventsExt = {
 color: '#f00',
 textColor: 'yellow',
 events: [
 {type:'party',title: 'Lunch 1',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
 {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
 {type:'party',title: 'Some class',start: new Date(y, m, d, 10, 0),end: new Date(y, m, d, 11, 0),allDay: false},
 {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
 ]
 };*/

/* event sources array
 $scope.eventSources = [];
 $scope.eventSources2 = [];
 */

/*
 var cambiarHoras = function(){
 $scope.filterData.filters.avoidHours.forEach(function(interval){
 if(interval!==undefined) {
 if(!Number.isInteger(interval.startTime)) {
 if (interval.startTime.getMinutes() > 0)
 interval.startTime = interval.startTime.getHours() + 0.5;
 else
 interval.startTime = interval.startTime.getHours()
 }
 if(interval.endTime instanceof Date) {
 console.log(interval.endTime);
 if (interval.endTime.getMinutes() > 0)
 interval.endTime = interval.endTime.getHours() + 0.5;
 else
 interval.endTime = interval.endTime.getHours();
 }
 }
 });
 };
 */

/* add custom event
 $scope.addEvent = function(title, start, end) {
 $scope.events.push({
 title: title,
 start: new Date(start.year, start.month, start.day, start.hour, start.minute),
 end: new Date(end.year, end.month, end.day, end.hour, end.minute),
 className: ['openSesame']
 });
 };
 */
/* remove event
 $scope.remove = function(index) {
 $scope.events.splice(index,1);
 };
 */
/* event source that calls a function on every view switch
 $scope.eventsF = function (start, end, timezone, callback) {
 var s = new Date(start).getTime() / 1000;
 var e = new Date(end).getTime() / 1000;
 var m = new Date(start).getMonth();
 var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
 callback(events);
 };
 */