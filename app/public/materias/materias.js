angular.module('horarios.materias', [])
    .controller('MateriasCtrl', ['$scope', 'object', '$http', function($scope, object, $http){
        $scope.filterData = object.get();
        $scope.carreras=$scope.filterData.carreras;
        $scope.planes = planes;

        $scope.$watch('filterData', function () {
            object.set($scope.filterData);
        },true);

        $scope.$watch('carreras', function () {
            $scope.carreras.forEach(function(carrera){
                if(carrera.name==="Todas/No aparece"){
                    getMaterias(carrera);
                }
            });
        });

        $scope.addRemoveMateria = function(materia){
            if(materia.enabled)
                $scope.filterData.courses.push(materia.key);
            else
                $scope.filterData.courses.splice($scope.filterData.courses.indexOf(materia.key),1);
        };

        var getMaterias = function(carrera){
            $http.get('https://horarios-itam-server.herokuapp.com/loadCourses').success(function(data){
                data.forEach(function(materia){
                    var arre = materia.split("-");
                    var depto = arre[0];
                    var key = arre[1];
                    var nombre = arre[2];
                    switch (depto){
                        case 'ACT':
                            $scope.planes[carrera.planes[0].index].semesters[0].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'ADM':
                            $scope.planes[carrera.planes[0].index].semesters[1].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'CLE':
                            $scope.planes[carrera.planes[0].index].semesters[11].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'COM':
                            $scope.planes[carrera.planes[0].index].semesters[2].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'CON':
                            $scope.planes[carrera.planes[0].index].semesters[3].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'CSO':
                            $scope.planes[carrera.planes[0].index].semesters[4].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'DER':
                            $scope.planes[carrera.planes[0].index].semesters[5].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'ECO':
                            $scope.planes[carrera.planes[0].index].semesters[6].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'EGN':
                            $scope.planes[carrera.planes[0].index].semesters[8].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'EIN':
                            $scope.planes[carrera.planes[0].index].semesters[9].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'EST':
                            $scope.planes[carrera.planes[0].index].semesters[7].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'IIO':
                            $scope.planes[carrera.planes[0].index].semesters[10].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'LEN':
                            $scope.planes[carrera.planes[0].index].semesters[11].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'MAT':
                            $scope.planes[carrera.planes[0].index].semesters[12].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                        case 'SDI':
                            $scope.planes[carrera.planes[0].index].semesters[13].classes.push({"name":nombre,"key":(depto+"-"+key),"enabled":false});
                            break;
                    }

                });

            });
        }

    }]);